<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\EmployeeController;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('create', [EmployeeController::class, 'create'])->name('create');
    Route::post('store', [EmployeeController::class, 'store'])->name('store');
    Route::get('employees', [EmployeeController::class, 'index'])->name('employees');
    Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('edit');
    Route::patch('/update/{id}', [EmployeeController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [EmployeeController::class, 'destroy'])->name('delete');
});