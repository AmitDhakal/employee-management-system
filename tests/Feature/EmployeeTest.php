<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;


class EmployeeTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function a_user_can_browse_employee_lists()
    {
        $response = $this->get('/employees');

        $response->assertStatus(200);
    }

    public function test_user_can_create_employee()
    {
        $formData = [
            'name' => 'john',
            'salary' => '50000',
            'age' => '25',
            'address' => 'Kathmandu',
            'phone_number' => '9808976541',
            'email' => 'john@gmail.com'
        ];

        $this->json("POST",route('store'), $formData)
            ->assertRedirect('/employees')
            ;
    }

    public function test_user_can_delete_employee()
    {
        //we can also generate unique id but it will be difficult to know which id to delete
        $user = User::factory()->create(['id' => 123]);

        $this->json("DELETE",route('delete',['id' => 123]))
            ->assertRedirect('/employees')
            ;
    }

}
