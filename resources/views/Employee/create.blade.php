@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/store" method="post">
        @csrf
        <div class="form-group">
          <label>Employee Name</label>
          <input type="text" name="name" class="form-control" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label>Email address</label>
            <input type="email" name="email" class="form-control" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" class="form-control" placeholder="Enter address">
          </div>
          <div class="form-group">
            <label>Salary</label>
            <input type="text" name="salary" class="form-control" placeholder="Enter Salary">
          </div>
          <div class="form-group">
            <label>Age</label>
            <input type="text" name="age" class="form-control" placeholder="Enter age">
          </div>
          <div class="form-group">
            <label>Phone Number</label>
            <input type="text" name="phone_number" class="form-control" placeholder="Enter Phone number">
          </div>

        <button type="submit" class="btn btn-primary">Create</button>
      </form>
</div>
@endsection
