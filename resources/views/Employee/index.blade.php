@extends('layouts.app')

@if(Session::has('flash_message'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <div>
    {{ Session::get('flash_message') }}
</div>
</div>
@endif


@section('content')

<table class="table">
  <a href={{route('create')}} style=”float: right”><button type="submit" class="btn btn-primary btn-md float-right">Add</button></a>
    <thead>
    @if($employees->count() <= 0)

      <tr class="alert alert-secondary text-float-center">
        <td colspan="14">There is no employee added yet. Please add some employee using Add button.</td>
    </tr>
    @else
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Address</th>
        <th scope="col">Salary</th>
        <th scope="col">Age</th>
        <th scope="col">Phone Number</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    @endif

    <tbody>
        @foreach ($employees as $employee)
            
      <tr>
        <th scope="row">{{ $employee->id }}</th>
        <td>{{ $employee->name }}</td>
        <td>{{ $employee->email }}</td>
        <td>{{ $employee->address }}</td>
        <td>{{ $employee->salary }}</td>
        <td>{{ $employee->age }}</td>
        <td>{{ $employee->phone_number }}</td>

        {{--  <form action="{{route('delete',[$employee->id])}}" method="POST">
            @method('DELETE')
            @csrf
            <button type="submit">Delete</button>               
        </form>  --}}
        <td>
            <div class="btn-group mr-2" role="group">
                {{-- <form action="{{route('edit',[$employee->id])}}" method="post">
                    @method('get')
                    @csrf
                    <button type="submit" class="btn btn-primary btn-sm">Edit</button>
                </form> --}}
                <a href={{route('edit',$employee->id)}}><button type="submit" class="btn btn-primary btn-sm">Edit</button></a>

                <form action="{{route('delete',['id' => $employee->id])}}" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </div>
        </td>
      </tr>
      @endforeach

    </tbody>
  </table>
@endsection
