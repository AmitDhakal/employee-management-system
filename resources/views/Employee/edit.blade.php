@extends('layouts.app')

@section('content')
<div class="container">
    <form action={{ route('update',['id' => $employee->id])}} method="post">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label>Employee Name</label>
          <input type="text" name="name" value={{ $employee->name }} class="form-control" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label>Email address</label>
            <input type="email" name="email" value={{ $employee->email }} class="form-control" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" value={{ $employee->address }} class="form-control" placeholder="Enter address">
          </div>
          <div class="form-group">
            <label>Salary</label>
            <input type="text" name="salary" value={{ $employee->salary }} class="form-control" placeholder="Enter Salary">
          </div>
          <div class="form-group">
            <label>Age</label>
            <input type="text" name="age" value={{ $employee->age }} class="form-control" placeholder="Enter age">
          </div>
          <div class="form-group">
            <label>Phone Number</label>
            <input type="text" name="phone_number" value={{ $employee->phone_number }} class="form-control" placeholder="Enter Phone number">
          </div>

        <button type="submit" class="btn btn-primary">Update</button>
      </form>
</div>
@endsection
